# Compiler assignments: Lexical analyzer with `Flex`


## What is **Lex**?

> - **Lex** is a program generator designed for lexical processing of character input streams.
>    - It accepts a high-level, problem oriented specification for character string matching, and produces a program in a general purpose language which recognizes **regular expressions**.
> - The regular expressions are specified by the user in the source specifications given to **Lex**.
> - **Lex** generates a **deterministic finite automaton (DFA)** from the regular expressions in the source.
> - The **Lex** written code recognizes these expressions in an input stream and partitions the input stream into strings matching the expressions.  

(From http://dinosaur.compilertools.net/lex/)


## Goal 

- Generate a lexical analyzer for the subset of C language
    - Please refer to [SPEC.pdf](./SPEC.pdf) **page 33** for more details.

[Input example](./test/sample_testcase.txt):
```c
int main() {
  int a = 1;
  int b = -1;
  return a + b;
}
```

[Output example](./test/sample_testcase.ans):
```
#key:int
#id:main
#punc:(
#punc:)
#punc:{
1:int main() {
#key:int
#id:a
#op:=
#integer:1
#punc:;
2:  int a = 1;
#key:int
#id:b
#op:=
#integer:-1
#punc:;
3:  int b = -1;
#key:return
#id:a
#op:+
#id:b
#punc:;
4:  return a + b;
#punc:}
5:}
```


## Usage

We use `Flex`(Fast Lex) instead of Lex.

1. Use `flex` to generate `lex.yy.c` from [`scanner.l`](./scanner.l)
    ```
    $ flex scanner.l
    ```
2. Generate `scanner` program
    ```
    $ gcc -o scanner lex.yy.c -lfl
    ```
3. Test `scanner` program
    ```
    $ ./scanner < test.c
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) for more details.**
