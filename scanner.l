%{

#include <stdio.h>
#include <string.h>

enum TokenType {
    KEY, MACRO, ID, OP, PUNC, INTEGER, FLOAT, CHAR, STRING
};

void appendToken(char *token);
void printALineOfCode(void);
void printToken(enum TokenType tokenType, char *token);


char ALineOfCode[301];
int currentLine = 1;

char strbuf[301];

int flag_src_on = 1;
int flag_token_on = 1;


%}

%x STATE_STRING
%x STATE_COMMENT

%%

" "+ {appendToken(yytext);}
\t+ {appendToken(yytext);}
\n {appendToken(yytext); printALineOfCode();}

"for" {printToken(KEY, yytext); appendToken(yytext);}
"do" {printToken(KEY, yytext); appendToken(yytext);}
"while" {printToken(KEY, yytext); appendToken(yytext);}
"break" {printToken(KEY, yytext); appendToken(yytext);}
"continue" {printToken(KEY, yytext); appendToken(yytext);}
"if" {printToken(KEY, yytext); appendToken(yytext);}
"else" {printToken(KEY, yytext); appendToken(yytext);}
"return" {printToken(KEY, yytext); appendToken(yytext);}
"struct" {printToken(KEY, yytext); appendToken(yytext);}
"switch" {printToken(KEY, yytext); appendToken(yytext);}
"case" {printToken(KEY, yytext); appendToken(yytext);}
"default" {printToken(KEY, yytext); appendToken(yytext);}
"void" {printToken(KEY, yytext); appendToken(yytext);}
"int" {printToken(KEY, yytext); appendToken(yytext);}
"double" {printToken(KEY, yytext); appendToken(yytext);}
"float" {printToken(KEY, yytext); appendToken(yytext);}
"char" {printToken(KEY, yytext); appendToken(yytext);}
"const" {printToken(KEY, yytext); appendToken(yytext);}
"signed" {printToken(KEY, yytext); appendToken(yytext);}
"unsigned" {printToken(KEY, yytext); appendToken(yytext);}
"short" {printToken(KEY, yytext); appendToken(yytext);}
"long" {printToken(KEY, yytext); appendToken(yytext);}

"NULL" {printToken(MACRO, yytext); appendToken(yytext);}
"__COUNTER__" {printToken(MACRO, yytext); appendToken(yytext);}
"__LINE__" {printToken(MACRO, yytext); appendToken(yytext);}
"INT_MAX" {printToken(MACRO, yytext); appendToken(yytext);}
"INT_MIN" {printToken(MACRO, yytext); appendToken(yytext);}
"CHAR_MAX" {printToken(MACRO, yytext); appendToken(yytext);}
"CHAR_MIN" {printToken(MACRO, yytext); appendToken(yytext);}
"MAX" {printToken(MACRO, yytext); appendToken(yytext);}
"MIN" {printToken(MACRO, yytext); appendToken(yytext);}

[_[:alpha:]][_[:alnum:]]* {printToken(ID, yytext); appendToken(yytext);}

"+" {printToken(OP, yytext); appendToken(yytext);}
"-" {printToken(OP, yytext); appendToken(yytext);}
"*" {printToken(OP, yytext); appendToken(yytext);}
"/" {printToken(OP, yytext); appendToken(yytext);}
"%" {printToken(OP, yytext); appendToken(yytext);}
"++" {printToken(OP, yytext); appendToken(yytext);}
"--" {printToken(OP, yytext); appendToken(yytext);}
"<" {printToken(OP, yytext); appendToken(yytext);}
"<=" {printToken(OP, yytext); appendToken(yytext);}
">" {printToken(OP, yytext); appendToken(yytext);}
">=" {printToken(OP, yytext); appendToken(yytext);}
"==" {printToken(OP, yytext); appendToken(yytext);}
"!=" {printToken(OP, yytext); appendToken(yytext);}
"=" {printToken(OP, yytext); appendToken(yytext);}
"&&" {printToken(OP, yytext); appendToken(yytext);}
"||" {printToken(OP, yytext); appendToken(yytext);}
"!" {printToken(OP, yytext); appendToken(yytext);}
"&" {printToken(OP, yytext); appendToken(yytext);}
"|" {printToken(OP, yytext); appendToken(yytext);}

":" {printToken(PUNC, yytext); appendToken(yytext);}
";" {printToken(PUNC, yytext); appendToken(yytext);}
"," {printToken(PUNC, yytext); appendToken(yytext);}
"." {printToken(PUNC, yytext); appendToken(yytext);}
"[" {printToken(PUNC, yytext); appendToken(yytext);}
"]" {printToken(PUNC, yytext); appendToken(yytext);}
"(" {printToken(PUNC, yytext); appendToken(yytext);}
")" {printToken(PUNC, yytext); appendToken(yytext);}
"{" {printToken(PUNC, yytext); appendToken(yytext);}
"}" {printToken(PUNC, yytext); appendToken(yytext);}

[+-]?[[:digit:]]+    {printToken(INTEGER, yytext); appendToken(yytext);}

[+-]?(([0-9]*[.][0-9]+)|([0-9]+[.][0-9]*)) {printToken(FLOAT, yytext); appendToken(yytext);}



\" {BEGIN STATE_STRING; strcpy(strbuf, "\"");} 
<STATE_STRING>\\[abefnrtv0] {strcat(strbuf, yytext);}
<STATE_STRING>\\\\ {strcat(strbuf, yytext);}
<STATE_STRING>\\' {strcat(strbuf, yytext);}
<STATE_STRING>\\\" {strcat(strbuf, yytext);}
<STATE_STRING>\\\? {strcat(strbuf, yytext);}
<STATE_STRING>\\[0-7]{1,3} {strcat(strbuf, yytext);}
<STATE_STRING>\\x[0-9A-Fa-f]+ {strcat(strbuf, yytext);}
<STATE_STRING>\\u[0-9A-Fa-f]{4,4} {strcat(strbuf, yytext);}
<STATE_STRING>\\U[0-9A-Fa-f]{8,8} {strcat(strbuf, yytext);}

<STATE_STRING>\" {BEGIN(INITIAL); strcat(strbuf, yytext); printToken(STRING, strbuf); appendToken(strbuf);}

<STATE_STRING>. {strcat(strbuf, yytext);}


'\\[abefnrtv0]' {printToken(CHAR, yytext); appendToken(yytext);}
'\\\\' {printToken(CHAR, yytext); appendToken(yytext);}
'\\'' {printToken(CHAR, yytext); appendToken(yytext);}
'\\\"' {printToken(CHAR, yytext); appendToken(yytext);}
'\\\?' {printToken(CHAR, yytext); appendToken(yytext);}
'\\[0-7]{1,3}' {printToken(CHAR, yytext); appendToken(yytext);}
'\\x[0-9A-Fa-f]+' {printToken(CHAR, yytext); appendToken(yytext);}
'\\u[0-9A-Fa-f]{4,4}' {printToken(CHAR, yytext); appendToken(yytext);}
'\\U[0-9A-Fa-f]{8,8}' {printToken(CHAR, yytext); appendToken(yytext);}
'.' {printToken(CHAR, yytext); appendToken(yytext);}



\/\/.*\n {appendToken(yytext); printALineOfCode();}

"/*" {BEGIN STATE_COMMENT; appendToken(yytext);}
<STATE_COMMENT>. {appendToken(yytext);}
<STATE_COMMENT>\n {appendToken(yytext); printALineOfCode();}
<STATE_COMMENT>"*/" {appendToken(yytext); BEGIN(INITIAL);}

[ \t]*#pragma[ \t]+source[ \t]+on[ \t]*\n {flag_src_on = 1; appendToken(yytext); printALineOfCode();}
[ \t]*#pragma[ \t]+source[ \t]+off[ \t]*\n {flag_src_on = 0; appendToken(yytext); printALineOfCode();}
[ \t]*#pragma[ \t]+token[ \t]+on[ \t]*\n {flag_token_on = 1; appendToken(yytext); printALineOfCode();}
[ \t]*#pragma[ \t]+token[ \t]+off[ \t]*\n {flag_token_on = 0; appendToken(yytext); printALineOfCode();}


%%

int main(int argc, char* argv[]){
    ALineOfCode[0] = '\0';
    yylex();
    return 0;
}

void appendToken(char *token){
    strcat(ALineOfCode, token);
}

void printALineOfCode(void){
    if (flag_src_on){
        printf("%d:%s", currentLine, ALineOfCode);
    }
    currentLine++;
    ALineOfCode[0] = '\0';
}

void printToken(enum TokenType tokenType, char *token){
    if (flag_token_on){
        switch(tokenType){
            case KEY:
                    printf("#key:%s\n", token);
                break;
            case MACRO:
                    printf("#macro:%s\n", token);
                break;
            case ID:
                    printf("#id:%s\n", token);
                break;
            case OP:
                    printf("#op:%s\n", token);
                break;
            case PUNC:
                    printf("#punc:%s\n", token);
                break;
            case INTEGER:
                    printf("#integer:%s\n", token);
                break;
            case FLOAT:
                    printf("#float:%s\n", token);
                break;
            case CHAR:
                    printf("#char:%s\n", token);
                break;
            case STRING:
                    printf("#string:%s\n", token);
                break;
        }
    }
}



// eliminate warning of unused variables warning...
int (*input_funcPointer)(void) = input;
void (*yyunput_funcPointer)(int, char*) = yyunput;
